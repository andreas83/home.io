require('./bootstrap');
window.Vue = require('vue');
window.Pusher = require('pusher-js');

import Vue from 'vue'
import Vuex from 'vuex'
import VueI18n from 'vue-i18n'
import BootstrapVue from 'bootstrap-vue'
import i18n from './i18n'




//font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faKey } from '@fortawesome/free-solid-svg-icons'
import { faBroadcastTower } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faBars)
library.add(faUser)
library.add(faKey)
library.add(faBroadcastTower)

//Vue Routing
import VueRouter from 'vue-router'
import Layout from './components/Layout'


import axios from 'axios';


Vue.use(VueI18n)
Vue.use(Vuex);
Vue.use(BootstrapVue);
Vue.use(VueRouter);

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('sensor-datatable', require('./components/sensorData/Table').default);
Vue.component('datasource-datatable', require('./components/dataSource/Table').default);
Vue.component('LineChart', require('./components/charts/Line').default);
Vue.component('BarChart', require('./components/charts/Bar').default);
Vue.component('TextLabel', require('./components/charts/TextLabel').default);
Vue.component('Liquid', require('./components/charts/Liquid').default);


import {router} from './routes.js';

  import { store } from './store/store.js';



    const app = new Vue({
        el: '#app',
        components: { Layout },
        store: store,
        router,
        i18n,
        data() {
          return{
              style:"clean"
          }

        },
        mounted(){

        },
        updated(){

        },
        methods:{

          onStyleChange(key){
            this.style=key;
            var body = document.body;
            body.classList.remove("clean");
            body.classList.remove("black");
            body.classList.add(this.style);
          }
        }
    });
