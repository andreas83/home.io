//Vue Routing
import VueRouter from 'vue-router'
import { store } from './store/store.js';
import Layout from './components/Layout'
import Welcome from './components/Welcome'
import ListSensor from './components/sensor/ListSensor'
import EditSensor from './components/sensor/EditSensor'
import ShowDashboard from './components/dashboard/ShowDashboard'
import ListDashboard from './components/dashboard/ListDashboard'
import EditDashboard from './components/dashboard/EditDashboard'
import ConfigureSensor from './components/dashboard/ConfigureSensor'
import EditDataSource from './components/dataSource/EditDataSource'
import ListDataSource from './components/dataSource/ListDataSource'
import LiveView from './components/sensorData/LiveView'
import Login from './components/auth/Login'
import Register from './components/auth/Register'
import Settings from './components/user/Settings'
  export const router = new VueRouter({
        mode: 'history',
        routes: [
            {
                path: '/',
                name: 'ShowWelcome',
                component: Welcome,
            },
            {
                path: '/login',
                name: 'Login',
                component: Login,
            },
            {
                path: '/register',
                name: 'Register',
                component: Register,
            },
            {
                path: '/register/canceled',
                name: 'Register',
                component: Register,
            },
            {
                path: '/register/success',
                name: 'Register',
                component: Register,
            },
            {
                path: '/user/settings',
                name: 'Settings',
                component: Settings,
                meta: {
                  requiresAuth: true
                }
            },
            {
                path: '/dashboard/:id',
                name: 'ShowDashboard',
                component: ShowDashboard,
                meta: {
                  requiresAuth: true
                }
            },
            {
                path: 'home',
                name: 'welcome',
                component: Welcome,
            },

            {
                path: '/list/sensor',
                name: 'ListSensor',
                component: ListSensor,
                meta: {
                  requiresAuth: true
                }
            },
            {
                path: '/sensor/edit/:id',
                name: 'EditSensor',
                component: EditSensor,
                meta: {
                  requiresAuth: true
                }
            },
            {
                path: '/live',
                name: 'LiveView',
                component: LiveView,
                meta: {
                  requiresAuth: true
                }

            },
            {
                path: '/list/data/source',
                name: 'ListDatasource',
                component: ListDataSource,
                meta: {
                  requiresAuth: true
                }

            },
            {
                path: '/data/source/edit/:id',
                name: 'EditDataSource',
                component: EditDataSource,
                meta: {
                  requiresAuth: true
                }

            },
            {
                path: '/list/dashboard',
                name: 'ListDashboard',
                component: ListDashboard,
                meta: {
                  requiresAuth: true
                }
            },
            {
                path: '/dashboard/edit/:id',
                name: 'EditDashboard',
                component: EditDashboard,
                meta: {
                  requiresAuth: true
                }
            },
            {
                path: '/dashboard/:dashboard_id/configure/item/:item_id',
                name: 'ConfigureSensor',
                component: ConfigureSensor,
                meta: {
                  requiresAuth: true
                }
            },
        ],
    })

    router.beforeEach((to, from, next) => {
      if(to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
          next()
          return
        }
        next('/login')
      } else {
        next()
      }
    })
