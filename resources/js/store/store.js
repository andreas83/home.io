import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { mapResourceModules } from '@reststate/vuex';


Vue.use(Vuex)
const token =localStorage.getItem('token') ;


const api = axios.create({
  baseURL: document.head.querySelector('meta[name="api-base-url"]').content,
  headers: {
    'Content-Type': 'application/vnd.api+json',
    'Authorization': `Bearer ${token}`,
  },
});

export const store = new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    user : {}
  },
  modules: {
      ...mapResourceModules({
      names: [
        'sensors',
        'dashboards',
        'sensorDatas',
        'dashboardItems',
        'dataSources',
      ],
      httpClient: api
    }),
  },
  mutations: {

    auth_request(state){
      state.status = 'loading'
    },

    store_user(state, user){
      state.user = user
    },

    auth_success(state, token){

      state.status = 'success'
      state.token = token

    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = ''
      state.token = ''
      state.user = {}
    },
  },
  actions: {
    login({commit}, user){
        return new Promise((resolve, reject) => {
          commit('auth_request')
          axios({url: '/login', data: user, method: 'POST' })
          .then(resp => {

            const token = resp.data.token
            const user = resp.data.user

            localStorage.setItem('token', token)

            axios.defaults.headers.common['Authorization'] = token

            api.interceptors.request.use(
              (config) => {
                config.headers['Authorization'] = "Bearer "+token;
                return config;
              },

              (error) => {
                return Promise.reject(error);
              }
            );
            commit('store_user', user)
            commit('auth_success', token )

            resolve(resp)
          })
          .catch(err => {

            commit('auth_error')
            localStorage.removeItem('token')
            reject(err.response)
          })

        })
    },
    register({commit}, user){
        return new Promise((resolve, reject) => {
          commit('auth_request')
          axios({url: '/register', data: user, method: 'POST' })
          .then(resp => {
            const token = resp.data.token
            const user = resp.data.user
            localStorage.setItem('token', token)
            axios.defaults.headers.common['Authorization'] = token


            commit('auth_success', token, user)

            resolve(resp)
          })
          .catch(err => {
            commit('auth_error', err)
            localStorage.removeItem('token')
            reject(err)
          })
        })
      },
      logout({commit}){
        return new Promise((resolve, reject) => {
          commit('logout')
          localStorage.removeItem('token')
          delete axios.defaults.headers.common['Authorization']
          resolve()
        })
      }
  },
  getters : {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    getUser: state => state.user,
  }
})
