<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSensorIdInFavorOfDatasourceId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
        
        Schema::table('dashboard_items', function (Blueprint $table) {
            $table->dropForeign('dashboard_items_sensor_id_foreign');
            $table->dropColumn('sensor_id');
            
            $table->bigInteger('datasource_id')->unsigned();
            $table->foreign('datasource_id')->references('id')->on('data_sources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
