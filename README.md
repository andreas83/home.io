# Home.io - Dashboarding / Monitoring APP 

## Preamble
The Project started as a monitoring / dashboarding app for my iot devices.

At current state it can be used to visualise every key/value pair. 
i.e. temp / 23 ° or response time to first byte / 200ms, so you can use it for a wild variety of use cases.   

 


# Features

* Creating unlimited Dashboards
* In depth configuration of Dashboard-Items (Color, Labels, Line, Bar, Fluid, Text)
* Complex datasources with nested conditions )
* Internationalisation ( English | German ) 
* Developer friendly (REST API)    

 # In progress

Websocket support for realtime updates (live view)

# planned features

* Alerting 
* More internationalisation ( Spanish, Chinese )

# Status 

Please be aware, this project in under heavy development and can be changed drastically.

## Dont use it for production, you have been warned!

## Screenshot

![](https://social.codejungle.org/upload/5de798250df085.12805301_Screenshot_20191204_112924.png)
![](https://social.codejungle.org/upload/5de79825420176.92579268_Screenshot_20191204_112957.png)
![](https://social.codejungle.org/upload/5de798255c7225.01746601_Screenshot_20191204_113054.png)
