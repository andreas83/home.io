<?php

use Illuminate\Http\Request;
use App\SensorData;
use App\Http\Controllers\Api;
use App\Scopes\UserScope;
use App\Http\Middleware\CheckPermissions;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

JsonApi::register('v1')->routes(function ($api) {
    Route::post('/register', 'API\AuthController@register');
    Route::post('/login', 'API\AuthController@login')->name("login");;
});

JsonApi::register('v1')->middleware('auth:api')->routes(function ($api) {


    Route::middleware('auth:api')->group(function () {
        Route::post('/logout', 'API\AuthController@logout');
    });
    $api->resource('sensors');
    $api->resource('sensorDatas');
    $api->resource('dashboards');
    $api->resource('dashboardItems');
    $api->resource('dataSources');


});

//since json api specification does not support
//aggregation we need to make it by ourself.

Route::get('api/v1/sensorDatas/{id}/data/key', function($id) {


    $datasource=\App\DataSource::withoutGlobalScope(UserScope::class)->select("query")->where("id", $id)->first();
    $qb=json_decode($datasource->query, true);


    $group = [];
    $group['query'] = ['children' => $qb['children']];
    $group['query']['logicalOperator'] = $qb['logicalOperator'];
    $method = $qb['logicalOperator'] === 'all' ? 'where' : 'orWhere';
    $sensorData = SensorData::select("key");
    $sensorData->addSelect('sensor_id');
    parseQBGroup($sensorData, $group, $method);
    return $sensorData->groupBy("key")->groupBy("sensor_id")->get();


});

//@TODO: can be removed soon (only needed for old sensors)
Route::post('api/sensors/data', function(Request $request) {
    return SensorData::create($request->all());
});

Route::post('api/endpoint/data', function(Request $request) {

    return SensorData::create($request->all());
})->middleware('auth:api', CheckPermissions::class);

//@TODO should be in a own helper class
function parseQBGroup($query, $group, $method = 'where')
{
    $query->{$method}(function ($subquery) use ($group) {
        $sub_method = $group['query']['logicalOperator'] === 'all' ? 'where' : 'orWhere';
        foreach ($group['query']['children'] as $child) {
            if ($child['type'] === 'query-builder-group') {
                parseQBGroup($subquery, $child, $sub_method);
            } else {
                parseQBRule($subquery, $child, $sub_method);
            }
        }
    });
}
function parseQBRule($query, $rule, $method)
{

    if ($rule['query']['rule'] === 'sensor_id') {
        $query->{$method}('sensor_id', $rule['query']['selectedOperator'], $rule['query']['value']);
    }
    if ($rule['query']['rule'] === 'key') {
        $query->{$method}('key', $rule['query']['selectedOperator'], $rule['query']['value']);
    }
    if ($rule['query']['rule'] === 'value') {
        $query->{$method}('value', $rule['query']['selectedOperator'], $rule['query']['value']);
    }
}

//
Route::post('api/v1/sensorDatas/query', function(Request $request) {
    $qb = $request->input('query');
    $page =(is_numeric($request->input('page')) ? $request->input('page') : 10) ;
    $group = [];
    $group['query'] = ['children' => $qb['children']];
    $group['query']['logicalOperator'] = $qb['logicalOperator'];
    $method = $qb['logicalOperator'] === 'all' ? 'where' : 'orWhere';
    $sensorData = \App\SensorData::query();
    parseQBGroup($sensorData, $group, $method);
    return $sensorData->paginate(10, ["key", "value", "created_at", "id"], "", $page);
});


Route::post('api/v1/sensorDatas/datasourID/{id}', function($id, Request $request) {


    $datasource=\App\DataSource::withoutGlobalScope(UserScope::class)->select("query")->where("id", $id)->first();
    $qb=json_decode($datasource->query, true);

    $page =(is_numeric($request->input('page')) ? $request->input('page') : 1) ;
    $limit =(is_numeric($request->input('limit')) ? $request->input('limit') : 300) ;

    $group = [];
    $group['query'] = ['children' => $qb['children']];
    $group['query']['logicalOperator'] = $qb['logicalOperator'];
    $method = $qb['logicalOperator'] === 'all' ? 'where' : 'orWhere';
    $sensorData = \App\SensorData::query();

    parseQBGroup($sensorData, $group, $method);
    return  $sensorData->orderby("created_at", "desc")->paginate($limit, ["key", "value", "created_at", "id", "sensor_id"], "", $page);


});
