<?php

namespace App\Observers;

use App\Dashboard;
use Illuminate\Support\Facades\Auth;

class DashboardObserver
{
    /**
     * Handle the dashboard "saving" event.
     *
     * @param  \App\Dashboard  $dashboard
     * @return void
     */
    public function saving(Dashboard $dashboard)
    {
        $dashboard->user_id=Auth::id();
    }

    /**
     * Handle the dashboard "updated" event.
     *
     * @param  \App\Dashboard  $dashboard
     * @return void
     */
    public function updated(Dashboard $dashboard)
    {
        //
    }

    /**
     * Handle the dashboard "deleted" event.
     *
     * @param  \App\Dashboard  $dashboard
     * @return void
     */
    public function deleted(Dashboard $dashboard)
    {
        //
    }

    /**
     * Handle the dashboard "restored" event.
     *
     * @param  \App\Dashboard  $dashboard
     * @return void
     */
    public function restored(Dashboard $dashboard)
    {
        //
    }

    /**
     * Handle the dashboard "force deleted" event.
     *
     * @param  \App\Dashboard  $dashboard
     * @return void
     */
    public function forceDeleted(Dashboard $dashboard)
    {
        //
    }
}
