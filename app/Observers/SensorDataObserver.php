<?php

namespace App\Observers;

use App\SensorData;
use App\Events\SensorDataUpdated;

class SensorDataObserver
{
    /**
     * Handle the sensor data "created" event.
     *
     * @param  \App\SensorData  $sensorData
     * @return void
     */
    public function created(SensorData $sensorData)
    {
    
        event(new SensorDataUpdated($sensorData));
    }

    /**
     * Handle the sensor data "updated" event.
     *
     * @param  \App\SensorData  $sensorData
     * @return void
     */
    public function updated(SensorData $sensorData)
    {
        //
    }

    /**
     * Handle the sensor data "deleted" event.
     *
     * @param  \App\SensorData  $sensorData
     * @return void
     */
    public function deleted(SensorData $sensorData)
    {
        //
    }

    /**
     * Handle the sensor data "restored" event.
     *
     * @param  \App\SensorData  $sensorData
     * @return void
     */
    public function restored(SensorData $sensorData)
    {
        //
    }

    /**
     * Handle the sensor data "force deleted" event.
     *
     * @param  \App\SensorData  $sensorData
     * @return void
     */
    public function forceDeleted(SensorData $sensorData)
    {
        //
    }
}
