<?php

namespace App\Observers;

use App\Sensor;
use Illuminate\Support\Facades\Auth;
class SensorObserver
{
    /**
     * Handle the sensor "created" event.
     *
     * @param  \App\Sensor  $sensor
     * @return void
     */
    public function created(Sensor $sensor)
    {
        //
    }

    /**
     * Handle the sensor "saving" event.
     *
     * @param  \App\Sensor  $sensor
     * @return void
     */
    public function saving(Sensor $sensor)
    {
        $sensor->user_id=Auth::id();
    }

    /**
     * Handle the sensor "updated" event.
     *
     * @param  \App\Sensor  $sensor
     * @return void
     */
    public function updated(Sensor $sensor)
    {
        //
    }

    /**
     * Handle the sensor "deleted" event.
     *
     * @param  \App\Sensor  $sensor
     * @return void
     */
    public function deleted(Sensor $sensor)
    {
        //
    }

    /**
     * Handle the sensor "restored" event.
     *
     * @param  \App\Sensor  $sensor
     * @return void
     */
    public function restored(Sensor $sensor)
    {
        //
    }

    /**
     * Handle the sensor "force deleted" event.
     *
     * @param  \App\Sensor  $sensor
     * @return void
     */
    public function forceDeleted(Sensor $sensor)
    {
        //
    }
}
