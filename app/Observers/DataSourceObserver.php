<?php

namespace App\Observers;

use App\DataSource;
use Illuminate\Support\Facades\Auth;

class DataSourceObserver
{
    /**
     * Handle the dataSource "saving" event.
     *
     * @param  \App\DataSource  $dataSource
     * @return void
     */
    public function saving(DataSource $dataSource)
    {
        $dataSource->user_id=Auth::id();
    }

    /**
     * Handle the dataSource "updated" event.
     *
     * @param  \App\DataSource  $dataSource
     * @return void
     */
    public function updated(DataSource $dataSource)
    {
        //
    }

    /**
     * Handle the dataSource "deleted" event.
     *
     * @param  \App\DataSource  $dataSource
     * @return void
     */
    public function deleted(DataSource $dataSource)
    {
        //
    }

    /**
     * Handle the dataSource "restored" event.
     *
     * @param  \App\DataSource  $dataSource
     * @return void
     */
    public function restored(DataSource $dataSource)
    {
        //
    }

    /**
     * Handle the dataSource "force deleted" event.
     *
     * @param  \App\DataSource  $dataSource
     * @return void
     */
    public function forceDeleted(DataSource $dataSource)
    {
        //
    }
}
