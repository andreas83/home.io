<?php

namespace App\Providers;
use App\Sensor;
use App\SensorData;
use App\Dashboard;
use App\DataSource;

use App\Observers\SensorObserver;
use App\Observers\DataSourceObserver;
use App\Observers\SensorDataObserver;
use App\Observers\DashboardObserver;


use CloudCreativity\LaravelJsonApi\LaravelJsonApi;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        SensorData::observe(SensorDataObserver::class);
        Dashboard::observe(DashboardObserver::class);
        Sensor::observe(SensorObserver::class);
        DataSource::observe(DataSourceObserver::class);
        LaravelJsonApi::defaultApi('v1');

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
