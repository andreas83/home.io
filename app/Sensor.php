<?php

namespace App;
use App\Scopes\UserScope;

use Illuminate\Database\Eloquent\Model;


class Sensor extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'location', 'description',
    ];

    protected $guarded = ['id', 'user_id'];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UserScope);
    }

}
