<?php

namespace App\Http\Middleware;

use Closure;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sensor = App\Sensor::find($request->sensor_id);
        if(Auth::user()->id != $sensor->user_id)
        {
            return route('login');
        }
        return $next($request);
    }
}
