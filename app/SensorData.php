<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorData extends Model
{

    protected $table='sensor_datas';

    protected $fillable = [
        'sensor_id', 'key', 'value',
    ];
}
