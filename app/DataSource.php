<?php

namespace App;
use App\Scopes\UserScope;
use Illuminate\Database\Eloquent\Model;


class DataSource extends Model
{
    

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'query', 'description', 'count', 'runtime',
    ];

    protected $guarded = ['id', 'user_id'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UserScope);
        
    }

    public function setQueryAttribute($value)
    {
        $this->attributes['query'] = json_encode($value);
    }


}
