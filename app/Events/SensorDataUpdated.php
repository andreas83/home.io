<?php

namespace App\Events;

use App\SensorData;
use CloudCreativity\LaravelJsonApi\Broadcasting\BroadcastsData;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SensorDataUpdated implements ShouldBroadcast
{

    use SerializesModels, BroadcastsData;

    protected $broadcastApi = 'v1';

    public $sensorData;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SensorData $sensor)
    {
        $this->sensorData=$sensor;
    }

    /**
     * @return array
     */
    public function broadcastOn()
    {
      return [new Channel('public')];
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'sensor.data.updated';
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
      return $this->serializeData($this->sensorData, ["key", "value", "sensor_id"]);
    }
}
