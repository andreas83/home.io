<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'label', 'datasource_id', 'dashboard_id', 'chart_key', 'sensor_data_key' , "size"
    ];


    public function setSensorDataKeyAttribute($value)
    {
        $this->attributes['sensor_data_key'] = json_encode($value);
    }

}
